#include "Polygon.h"
#include "Texture.h"

#include <gl\gl.h>
#include <stdio.h>
#pragma warning (disable: 4996)

Vertex::Vertex()
: x(-100)
, y(-100)
, checked(false)
{}

PolygonManager::PolygonManager()
{}

PolygonManager::~PolygonManager()
{}

int PolygonManager::CalcOrientation(Poly &poly)
{
	int square = 0;
	int x1, y1, x2, y2;

	size_t last = poly.size() - 1;
	for (size_t i = 0; i < last; ++i) {
		x1 = poly[i].x;
		y1 = poly[i].y;
		x2 = poly[i + 1].x;
		y2 = poly[i + 1].y;
		square += (x2 - x1) * (y2 + y1);
	}
	x1 = poly[last].x;
	y1 = poly[last].y;
	x2 = poly[0].x;
	y2 = poly[0].y;
	square += (x2 - x1) * (y2 + y1);
	if (square == 0) {
		return 0;
	}
	return square > 0 ? 1 : -1;
}

bool PolygonManager::PolySelected(float x, float y, Poly &poly)
{
	int size = (int)poly.size();

	bool parity = false;
	int i, j;
	int xi, yi, xj, yj;
	for(i = 0, j = size - 1; i < size; j = i++) {
		xi = poly[i].x; yi = poly[i].y;
		xj = poly[j].x; yj = poly[j].y;

		if (((yi < y && y <= yj) || (yj < y && y <= yi)) && (x > (xj - xi) * (y - yi) / (yj - yi) + xi)) {
			parity = !parity;
		}
	}

	return parity;
}

void PolygonManager::LoadPolygons(Texture *tex, float x, float y)
{
	if (tex == nullptr) {
		return;
	}

	std::string filename = tex->name;
	filename.replace(filename.size() - 3, 3, "poly");

	Polygons.clear();
	Orientation.clear();

	FILE *f = fopen(filename.c_str(), "rt");
	if (f != nullptr) {
		size_t num_polys = 0;
		fscanf(f, "polygons: %i\n", &num_polys);
		for (size_t j = 0; j < num_polys; ++j) {
			Poly poly;
			size_t num_verts = 0;
			fscanf(f, "vertices: %i\n", &num_verts);
			for (size_t i = 0; i < num_verts; ++i) {
				Vertex vert;
				std::string fmt = "{\"x\":%i, \"y\":%i}";
				if (i == num_verts - 1) {
					fmt += "\n";
				}
				else {
					fmt += ",\n";
				}
				fscanf(f, fmt.c_str(), &vert.x, &vert.y);
				poly.push_back(vert);
			}
			Orientation.push_back(CalcOrientation(poly));
			Polygons.push_back(poly);
		}
		fclose(f);
	}

	Vertex vert;
	vert.x = (int)x;
	vert.y = (int)y;
	Poly poly;
	poly.push_back(vert);
	Polygons.push_back(poly);
}

void PolygonManager::SavePolygons(Texture *tex)
{
	if (tex == nullptr) {
		return;
	}
	int num_poly = int(Polygons.size()) - 1;
	if (num_poly < 1) return;

	int curPolyId = 0;
	do
	{
		std::vector< Vertex > *cur_poly = &Polygons[curPolyId];
		if (cur_poly->empty())
		{
			Polygons.erase(Polygons.begin() + curPolyId);
		}
		else curPolyId++;
	}
	while (!Polygons.empty() && curPolyId < int(Polygons.size()) - 1);

	num_poly = int(Polygons.size()) - 1;
	if (num_poly < 1) {
		return;
	}

	std::string filename = tex->name;
	filename.replace(filename.size() - 3, 3, "poly");

	FILE *f = fopen(filename.c_str(), "wt");
	if (f != nullptr) {
		fprintf(f, "polygons: %i\n", num_poly);
		for (int j = 0; j < num_poly; ++j) {
			Poly &poly = Polygons[j];
			size_t num_verts = poly.size();
			fprintf(f, "vertices: %i\n", num_verts);
			for (size_t i = 0; i < num_verts; ++i) {
				std::string fmt = "{\"x\":%i, \"y\":%i}";
				if (i == num_verts - 1) {
					fmt += "\n";
				}
				else {
					fmt += ",\n";
				}
				fprintf(f, fmt.c_str(), poly[i].x, poly[i].y);
			}
		}
		fclose(f);
	}
}

void PolygonManager::EraseSelected(float x, float y)
{
	int last = (int)Polygons.size() - 1;
	for (int j = last; j >= 0; --j) {
		Poly &poly = Polygons[j];
		if (PolySelected(x, y, poly)) {
			Polygons.erase(Polygons.begin() + j);
			Orientation.erase(Orientation.begin() + j);
			break;
		}
	}
}

void PolygonManager::EraseCheckedVertex()
{
	for (auto &poly : Polygons) {
		size_t size = poly.size();
		for (size_t i = 0; i < size; ++i) {
			auto &vert = poly[i];
			if (vert.checked) {
				poly.erase(poly.begin() + i);
				if (poly.size() == 2) {
					poly.clear();
				}
				return;
			}
		}
	}
}

void PolygonManager::AddVertex(float x, float y, double rect_size)
{
	bool add_new = false;
	Poly &last_poly = Polygons.back();
	Vertex vert;
	vert.x = (int)x;
	vert.y = (int)y;
	if (last_poly.size() > 1) {
		Vertex last_vert = last_poly.back();
		Vertex first_vert = last_poly.front();
		double rect[4] = { 
			last_vert.x - rect_size, last_vert.y - rect_size,
			last_vert.x + rect_size, last_vert.y + rect_size
		};
		if (first_vert.x >= rect[0] && first_vert.y >= rect[1] &&
			first_vert.x <  rect[2] && first_vert.y <  rect[3]) {
			last_poly.erase(last_poly.end() - 1);
			Orientation.push_back(CalcOrientation(last_poly));
			Poly poly;
			poly.push_back(vert);
			Polygons.push_back(poly);
			add_new = true;
		}
	}
	if (!add_new) {
		bool checked = false;
		for (auto &vert : last_poly) {
			checked = vert.checked;
			if (checked) {
				break;
			}
		}
		if (!checked) {
			last_poly.push_back(vert);
		}
	}
}

void PolygonManager::UpdateLastVertex(float x, float y)
{
	Poly &poly = Polygons.back();
	auto &vert = poly.back();
	vert.x = (int)x;
	vert.y = (int)y;
}

void PolygonManager::Reset(float x, float y)
{
	Orientation.clear();
	Polygons.clear();
	Vertex vert;
	vert.x = (int)x;
	vert.y = (int)y;
	Poly poly;
	poly.push_back(vert);
	Polygons.push_back(poly);
}

bool PolygonManager::InverseSelected(float x, float y)
{
	size_t last = Polygons.size() - 1;
	for (size_t j = 0; j < last; ++j) {
		Poly &poly = Polygons[j];
		if (PolySelected(x, y, poly)) {
			std::reverse(poly.begin(), poly.end());
			Orientation[j] = CalcOrientation(poly);
			return true;
		}
	}
	return false;
}

void PolygonManager::EraseTwoLastVertices()
{
	auto &lastPoly = Polygons.back();
	if (lastPoly.size() > 1) {
		lastPoly.erase(lastPoly.end() - 2);
	}
}

void PolygonManager::RedrawPolygons(float x, float y, double rect_size, bool edit, bool button_down, long &drag_x, long &drag_y)
{
	// draw lines
	glEnable(GL_LINE_SMOOTH);
	size_t num_polys = Polygons.size();
	for (size_t j = 0; j < num_polys; ++j) {
		float lines_color[] = { 0.0f, 1.0f, 0.0f, 1.0f };
		if (num_polys - 1 == j) {
			glLineWidth(2.0);
			glBegin(GL_LINE_STRIP);
			lines_color[0] = 0.7f;
		}
		else {
			if (Orientation[j] == -1) {
				float swap = lines_color[0];
				lines_color[0] = lines_color[1];
				lines_color[1] = swap;
			}
			if (j != num_polys - 1 && PolySelected(x, y, Polygons[j])) {
				glLineWidth(4.0);
				glBegin(GL_LINE_LOOP);
			}
			else {
				glLineWidth(2.0);
				glBegin(GL_LINE_LOOP);
			}
		}
		glColor4fv(lines_color);
		Poly &poly = Polygons[j];
		size_t poly_size = poly.size();
		if (edit && j == num_polys - 1) {
			poly_size--;
		}
		for (auto &vert : poly) {
			glVertex2i(vert.x, vert.y);
		}
		glEnd();
	}
	glDisable(GL_LINE_SMOOTH);

	// draw points
	for (size_t j = 0; j < num_polys; ++j) {
		Poly &cur_poly = Polygons[j];
		size_t poly_size = cur_poly.size();
		for (size_t i = 0; i < poly_size; ++i) {
			Vertex &vert = cur_poly[i];
			if (i == poly_size - 1 && j == num_polys - 1) {
				float border_color[] = { 1.0f, 1.0f, 0.0f };
				if (edit) {
					border_color[1] = 0.5f;
				}
				glColor3fv(border_color);
				glLineWidth(1.0);
				glBegin(GL_LINE_LOOP);
					glVertex2d(vert.x - rect_size, vert.y + rect_size);
					glVertex2d(vert.x - rect_size, vert.y - rect_size);
					glVertex2d(vert.x + rect_size, vert.y - rect_size);
					glVertex2d(vert.x + rect_size, vert.y + rect_size);
				glEnd();
			}
			else {
				Vertex last_vert = Polygons.back().back();
				double rect[4] = { 
					last_vert.x - rect_size, last_vert.y - rect_size,
					last_vert.x + rect_size, last_vert.y + rect_size 
				};
				float color[3] = {1, 0, 0};
				if (j != num_polys - 1) {
					if (Orientation[j] == -1) {
						float swap = color[0];
						color[0] = color[1];
						color[1] = swap;
					}
				}
				if ((vert.x >= rect[0] && vert.y >= rect[1] && vert.x <  rect[2] && vert.y <  rect[3]) ||
					(drag_x == j && drag_y == i)) {
					vert.checked = true;
					if (j != num_polys - 1) {
						int swap = Orientation[j] == -1 ? 0 : 1;
						color[swap] += 1.0f;
						color[2] = color[swap];
					}
					glColor3fv(color);
					if (button_down && edit) {
						drag_x = j;
						drag_y = i;
						vert.x = (int)x;
						vert.y = (int)y;
						Orientation[j] = CalcOrientation(cur_poly); 
					}
				}
				else {
					vert.checked = false;
					glColor3fv(color);
				}
				glBegin(GL_POINTS);
				glVertex2d(vert.x, vert.y);
				glEnd();
			}
		}
	}
}
