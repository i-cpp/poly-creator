#pragma once

#include <string>
#include <vector>

struct Texture {
	unsigned int id;
	std::string name;
	int width; 
	int height;
	int pot_width; 
	int pot_height;
	int bpp;
};

class TextureManager {
public:
	TextureManager();
	~TextureManager();
	unsigned int LoadTextures(std::string folder);

	Texture* GetTexture(size_t id);
	bool HasTexture(size_t id);
	void DrawTexture(size_t id);

	size_t LastTexture();

private:
	unsigned int LoadTexture(const char *filename);

	std::vector<Texture> Textures;
	
};
