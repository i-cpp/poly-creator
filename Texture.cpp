#include "Texture.h"

#include <windows.h>
#include <gl\gl.h>
#include <png.h>
#include <zlib.h>
#include <algorithm>

extern "C" { FILE __iob_func[3] = { *stdin,*stdout,*stderr }; }

#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "libpng.lib")
#pragma comment(lib, "zlib.lib")

#define NEXT_POW_OF_TWO(value, result) { result = 1; while (result < value) result <<= 1; }

TextureManager::TextureManager()
{}

TextureManager::~TextureManager()
{
	for (auto &tex : Textures) {
		if (glIsTexture(tex.id)) {
			glDeleteTextures(1, &tex.id);
		}
	}
}

unsigned int TextureManager::LoadTextures(std::string folder)
{
	HANDLE hFind;
	WIN32_FIND_DATAA FindFileData;
	std::string addr = folder + "\\*.png";
	hFind = FindFirstFileA(addr.c_str(), &FindFileData);
	if (hFind != INVALID_HANDLE_VALUE) {
		if(!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) && 
			strcmp(FindFileData.cFileName, "..") && 
			strcmp(FindFileData.cFileName, ".")) {
			std::string buf = folder + "\\" + FindFileData.cFileName;
			LoadTexture(buf.c_str());
		}
		while(FindNextFileA(hFind, &FindFileData)) {
			if(!(FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) 
				&& strcmp(FindFileData.cFileName, "..")
				&& strcmp(FindFileData.cFileName, ".")) {
				std::string buf = folder + "\\" + FindFileData.cFileName;
				LoadTexture(buf.c_str());
			}
		}
	}
	FindClose(hFind);
	return Textures.size();
}

size_t TextureManager::LastTexture()
{
	return Textures.size() - 1;
}

bool TextureManager::HasTexture(size_t id)
{
	return Textures.size() > id;
}

Texture* TextureManager::GetTexture(size_t id)
{
	if (Textures.size() <= id) {
		return nullptr;
	}
	return &Textures.at(id);
}

unsigned int TextureManager::LoadTexture(const char *filename)
{
	auto found = std::find_if(Textures.begin(), Textures.end(), [=](Texture &t) {
		return t.name == filename;
	});
	if (found != Textures.end()) {
		return found->id;
	}

	unsigned char *pixels = nullptr;
	Texture tex;
	tex.id = 0;
	tex.name = filename;

	const char *ext = filename + strlen(filename) - 4;
	if (!strcmp(ext, ".png")) {
		png_image image;
		memset(&image, 0, (sizeof image));
		image.version = PNG_IMAGE_VERSION;
		if (png_image_begin_read_from_file(&image, filename)) {
			tex.bpp = PNG_IMAGE_SAMPLE_CHANNELS(image.format);
			image.format = tex.bpp == 4 ? PNG_FORMAT_RGBA : PNG_FORMAT_RGB;

			tex.width = image.width;
			tex.height = image.height;
			size_t size = PNG_IMAGE_SIZE(image);
			pixels = (unsigned char*)malloc(size);

			png_image_finish_read(&image, 0/*background*/, pixels, 0/*row_stride*/, 0/*colormap*/);
			png_image_free(&image);
		}
	}

	if (pixels != nullptr) {
		NEXT_POW_OF_TWO(tex.width, tex.pot_width);
		NEXT_POW_OF_TWO(tex.height, tex.pot_height);
		char *expanded_data = (char*)malloc(tex.pot_width * tex.pot_height * tex.bpp);
		if (expanded_data != nullptr) {
			for (int y = 0; y < tex.height; ++y) {
				memcpy(&expanded_data[tex.pot_width * tex.bpp * y], &pixels[tex.width * tex.bpp * y], tex.width * tex.bpp);
			}
			glEnable(GL_TEXTURE_2D);
			glGenTextures(1, &tex.id);
			glBindTexture(GL_TEXTURE_2D, tex.id);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glTexImage2D(GL_TEXTURE_2D, 0, tex.bpp == 4 ? GL_RGBA : GL_RGB, tex.pot_width, tex.pot_height, 0, 
				tex.bpp == 4 ? GL_RGBA : GL_RGB, GL_UNSIGNED_BYTE, expanded_data);
			free(expanded_data);

			Textures.push_back(tex);
		}
		free(pixels);
	}
	return tex.id;
}

void TextureManager::DrawTexture(size_t id) 
{
	if (Textures.size() > id) {
		Texture *tex = &Textures.at(id);
		if (glIsTexture(tex->id)) {

			glEnable(GL_TEXTURE_2D);
			glColor3f(1, 1, 1);
			glBindTexture(GL_TEXTURE_2D, tex->id);
			
			float tex_w = (float)tex->width / tex->pot_width;
			float tex_h = (float)tex->height / tex->pot_height;

			glBegin(GL_QUADS);
				glTexCoord2f(0, tex_h);
				glVertex2i(0, tex->height);
				
				glTexCoord2f(0, 0);
				glVertex2i(0, 0);
				
				glTexCoord2f(tex_w, 0);
				glVertex2i(tex->width, 0);
				
				glTexCoord2f(tex_w, tex_h);
				glVertex2i(tex->width, tex->height);
			glEnd();

			glDisable(GL_TEXTURE_2D);
		}
	}
}