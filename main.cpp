#include "GLWindow.h"
#include "Texture.h"
#include "RasterFont.h"
#include "Polygon.h"

#include <gl\gl.h>
#include <crtdbg.h>

GLWindow *window = nullptr;
TextureManager *textureManager = nullptr;
PolygonManager *polygonManager = nullptr;

#define GET_TEX textureManager->GetTexture(currentState.current_image)

void RenderScene();
void UpdateWindowRect();

struct State {
	bool button_down;
	bool button_up;
	bool edit_mode;
	POINT drag;
	POINT cursor;
	size_t current_image;
	float font_color[3];
	float scale;
	float anchor_x;
	float anchor_y;
	float cur_x;
	float cur_y;
	int scale_orientation;
	bool ctrl;
};
State currentState;

void ResetState()
{
	currentState.button_down = false;
	currentState.button_up = false;
	currentState.edit_mode = false;
	currentState.drag.x = currentState.drag.y = -1;
	currentState.cursor.x = currentState.cursor.y = 0;
	currentState.current_image = 0;
	currentState.font_color[0] = 0;
	currentState.font_color[1] = 0;
	currentState.font_color[2] = 0;
	currentState.scale = 1.0f;
	currentState.anchor_x = currentState.anchor_y = 0.5f;
	currentState.cur_x = currentState.cur_y = 0;
	currentState.scale_orientation = 0;
	currentState.ctrl = false;
}

void UpdateCursorLocation()
{
	Texture *tex = GET_TEX;
	currentState.cur_x = currentState.cur_y = 0;
	if (tex != nullptr) {
		float anchor_x = tex->width * currentState.anchor_x;
		float anchor_y = tex->height * currentState.anchor_y;
		currentState.cur_x = (currentState.cursor.x - anchor_x) / currentState.scale + anchor_x;
		currentState.cur_y = (currentState.cursor.y - anchor_y) / currentState.scale + anchor_y;
	}
}

void UpdateScaleValue()
{
	if (currentState.scale_orientation != 0) {
		if (currentState.scale_orientation > 0) {
			currentState.scale = min(currentState.scale + 0.01f, 10);
		}
		else {
			currentState.scale = max(currentState.scale - 0.01f, 1);
		}
	}
	if (currentState.ctrl) {
		auto tex = GET_TEX;
		if (tex != nullptr) {
			currentState.anchor_x = (float)currentState.cursor.x / tex->width;
			currentState.anchor_y = (float)currentState.cursor.y / tex->height;
		}
	}
}

BOOL WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	int result = -1;

	window = new GLWindow();
	textureManager = new TextureManager();
	polygonManager = new PolygonManager();

	ResetState();
	
	if (window->Init(hInst, RenderScene)) {
		std::string folder = "images";
		if (textureManager->LoadTextures(folder) == 0) {
			window->ShowMessage("� ����� \"" + folder + "\" ��� ����������� ������� PNG!", "������");
		}
		else {
			UpdateWindowRect();

			polygonManager->LoadPolygons(GET_TEX, 0, 0);
			
			MSG msg = { 0 };
			while((msg.message != WM_QUIT)) {
				if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
					TranslateMessage(&msg);
					DispatchMessage(&msg);
				}
				else {
					if (window != nullptr) {
						window->Redraw();
					}
					currentState.button_up = false;
				}
			}
			result = msg.wParam;
		}
	}

	delete polygonManager;
	delete textureManager;
	delete window;
	
	_CrtDumpMemoryLeaks();
	return result;
}

void UpdateWindowRect()
{
	Texture *tex = GET_TEX;
	if (tex != nullptr) {
		window->Resize(tex->width, tex->height);
	}
}

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
		case WM_ACTIVATE:
			currentState.scale_orientation = 0;
		break;
		case WM_CLOSE:
			PostQuitMessage(0);
		return 0;
		case WM_KEYDOWN: {
			if (wParam == 187) { // +
				currentState.scale_orientation = 1;
			}
			else if (wParam == 189) { // -
				currentState.scale_orientation = -1;
			}
			else if (wParam == VK_CONTROL) { // Ctrl
				currentState.ctrl = true;
			}
		}
		break;
		case WM_KEYUP: {
			if (wParam == 80) { // P
				polygonManager->EraseSelected(currentState.cur_x, currentState.cur_y);
			}
			if (wParam == 187 || wParam == 189) { // + / -
				currentState.scale_orientation = 0;
			}
			else if (wParam == 68) { // D
				polygonManager->EraseTwoLastVertices();
			}
			else if (wParam == 73) { // I
				polygonManager->InverseSelected(currentState.cur_x, currentState.cur_y);
			}
			else if (wParam == 46) { // Delete
				polygonManager->EraseCheckedVertex();
			}
			else if (wParam == 69) { // E
				currentState.edit_mode = !currentState.edit_mode;
			}
			else if (wParam == 70) { // F
				// inverse font color
				for (int i = 0; i < 3; i++) {
					currentState.font_color[i] = 1 - currentState.font_color[i];
				}
			}
			else if (wParam == 116) { // F5
				polygonManager->SavePolygons(GET_TEX);
			}
			else if (wParam == 120) { // F9
				polygonManager->LoadPolygons(GET_TEX, currentState.cur_x, currentState.cur_y);
			}
			else if (wParam == 67) { // C
				polygonManager->Reset(currentState.cur_x, currentState.cur_y);
			}
			else if (wParam == 27) { // ESC
				PostQuitMessage(0);
				return 0;
			}
			else if (wParam == VK_CONTROL) { // Ctrl
				currentState.ctrl = false;
			}
			else if (wParam == 38 || wParam == 39) { // right or up
				polygonManager->SavePolygons(GET_TEX);
				currentState.current_image++;
				if (!textureManager->HasTexture(currentState.current_image)) {
					currentState.current_image = 0;
				}
				polygonManager->LoadPolygons(GET_TEX, currentState.cur_x, currentState.cur_y);
				UpdateWindowRect();
				currentState.scale = 1.f;
			}
			else if (wParam == 37 || wParam == 40) { // left or down
				polygonManager->SavePolygons(GET_TEX);
				if (currentState.current_image == 0) {
					currentState.current_image = textureManager->LastTexture();
				}
				else {
					currentState.current_image--;
				}
				polygonManager->LoadPolygons(GET_TEX, currentState.cur_x, currentState.cur_y);
				UpdateWindowRect();
				currentState.scale = 1.f;
			}
		}
		break;
		case WM_LBUTTONDOWN:
			currentState.button_down = true;
		break;
		case WM_LBUTTONUP: {
			currentState.button_down = false;
			currentState.drag.x = -1;
			currentState.drag.y = -1;
			currentState.button_up = true;
		}
		break;
		case WM_MOUSEMOVE: {
			GetCursorPos(&currentState.cursor);
			ScreenToClient(hWnd, &currentState.cursor);
		}
		break;
		default: { /* do nothing */ }
	}
	return CallWindowProc((WNDPROC)DefWindowProc, hWnd, msg, wParam, lParam);
}

void PrintInformation()
{
	std::string msg;
	int row = 0;

	glColor3fv(currentState.font_color);
	msg = std::to_string(currentState.cursor.x) + " x " + std::to_string(currentState.cursor.y);
	RasterFont::PrintText8x13(20, 50 + (row+=20), msg.c_str(), 1);

	RasterFont::PrintText8x13(20, 50 + (row+=20), "Next image = press Right or Up", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Last image = press Left or Down", 1);

	Texture *tex = GET_TEX;
	if (tex != nullptr) {
		std::string msg = "Current Map = id: " + std::to_string(currentState.current_image) + " name: " + tex->name;
		RasterFont::PrintText8x13(20, 50 + (row+=20), msg.c_str(), 1);
	}

	RasterFont::PrintText8x13(20, 50 + (row+=20), "Inverse font color = press 'f'", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Load polygons = press F9", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Save polygons = press F5", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Clear all = press 'c'", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Delete selected polygon = press 'p'", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Delete last point = press 'd'", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Delete selected point = press Delete", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Inverse orientation = press 'i'", 1);
	RasterFont::PrintText8x13(20, 50 + (row+=20), "Zoom image = press Ctrl and '+' or '-'", 1);

	msg = "Edit Polygons Mode = " + std::string(currentState.edit_mode ? "enebled" : "disebled") + " (press 'e')";
	RasterFont::PrintText8x13(20, 50 + (row+=20), msg.c_str(), 1);
}

void RenderScene()
{
	UpdateScaleValue();
	UpdateCursorLocation();
	polygonManager->UpdateLastVertex(currentState.cur_x, currentState.cur_y);

	float point_size = 5.0;
	double scaleBBox = 0.7 / currentState.scale;
	glPointSize(point_size);

	if (currentState.button_up && !currentState.edit_mode) {
		polygonManager->AddVertex(currentState.cur_x, currentState.cur_y, point_size * scaleBBox);
	}

	glPushMatrix();

	auto tex = GET_TEX;
	if (tex != nullptr) {
		// apply transforms
		glTranslatef(tex->width * currentState.anchor_x, tex->height * currentState.anchor_y, 0);
		glScalef(currentState.scale, currentState.scale, 1);
		glTranslatef(-tex->width * currentState.anchor_x, -tex->height * currentState.anchor_y, 0);
	}

	textureManager->DrawTexture(currentState.current_image);

	polygonManager->RedrawPolygons(currentState.cur_x, currentState.cur_y, point_size * scaleBBox, 
		currentState.edit_mode, currentState.button_down, currentState.drag.x, currentState.drag.y);
	
	glPopMatrix();

	PrintInformation();
}
