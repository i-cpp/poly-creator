#pragma once

namespace RasterFont {
	void PrintText8x13(int x, int y, const char *text, int interval);
};
