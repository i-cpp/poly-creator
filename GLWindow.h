#pragma once

#include <windows.h>
#include <string>

LRESULT CALLBACK MainWndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

class GLWindow
{
public:
	GLWindow();
	~GLWindow();
	bool Init(HINSTANCE hInst, void (*callback)());
	void Redraw();
	void Resize(int width, int height);
	void ShowMessage(std::string text, std::string caption);

private:
	void (*RenderSceneFunc)();

	int PixelFormat;
	HDC hDC;
	HGLRC hRC;
	HWND hMainWnd;
	HINSTANCE hInstance;
	DWORD WindowStyle;

};

 