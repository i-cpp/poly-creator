#include "GLWindow.h"

#include <gl\gl.h>

#define APP_NAME L"PolyCreator"
#define WINDOW_CLASS_NAME L"wcexPolyCreator"

GLWindow::GLWindow()
: hRC(0)
, hDC(0)
, PixelFormat(0)
, hMainWnd(0)
, hInstance(0)
, WindowStyle(0)
, RenderSceneFunc(nullptr)
{}

bool GLWindow::Init(HINSTANCE hInst, void (*callback)())
{
	if (callback != nullptr) {
		hInstance = hInst;
		RenderSceneFunc = callback;
		SIZE WindowSize = { 1024, 768 };
		WNDCLASSEX wcex = { 0 };
		wcex.cbSize		   = sizeof(WNDCLASSEXW);
		wcex.style         = CS_HREDRAW | CS_VREDRAW;
		wcex.lpfnWndProc   = MainWndProc;
		wcex.hInstance     = GetModuleHandle(0);
		wcex.hCursor	   = LoadCursor(0, IDC_ARROW);
		wcex.lpszClassName = WINDOW_CLASS_NAME;
		wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
		if(!RegisterClassEx(&wcex)) return false;

		WindowStyle = WS_CLIPCHILDREN | WS_POPUP | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX;
		hMainWnd = CreateWindowW(wcex.lpszClassName, APP_NAME, WindowStyle, 0, 0, 1, 1, 0, 0, hInstance, 0);

		if (hMainWnd) {	
			PIXELFORMATDESCRIPTOR pfd = { 0 };
			pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			pfd.nVersion = 1;
			pfd.dwFlags = PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER | PFD_DRAW_TO_WINDOW;
			pfd.iPixelType = PFD_TYPE_RGBA;
			pfd.cColorBits = 24;
			pfd.cDepthBits = 32;
			pfd.iLayerType = PFD_MAIN_PLANE;

			hDC = GetDC(hMainWnd);
			PixelFormat = ChoosePixelFormat (hDC, &pfd);
			if(PixelFormat != 0 && SetPixelFormat(hDC, PixelFormat, &pfd)) {
				hRC = wglCreateContext(hDC);
				if (hRC && wglMakeCurrent(hDC, hRC)) {
					glViewport(0, 0, WindowSize.cx, WindowSize.cy);
					glMatrixMode(GL_PROJECTION);
					glLoadIdentity();
					glOrtho(0, WindowSize.cx, WindowSize.cy, 0, -1.0, 1.0);
					glMatrixMode(GL_MODELVIEW);
					glLoadIdentity();

					glShadeModel(GL_SMOOTH);
					glEnable (GL_BLEND);
					glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
					glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
					glClearColor(0.53f, 0.67f, 0.83f, 1);
					glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
					
					glEnable(GL_POINT_SMOOTH);
					//glEnable(GL_LINE_SMOOTH);
					
					ShowWindow(hMainWnd, SW_NORMAL);
					UpdateWindow(hMainWnd);
					return true;
				}
			}
		}
	}
	return false;
}

void GLWindow::ShowMessage(std::string text, std::string caption)
{
	MessageBoxA(hMainWnd, text.c_str(), caption.c_str(), MB_OK);
}

void GLWindow::Resize(int width, int height)
{
	RECT rcWnd = {0, 0, width, height};
	rcWnd.left = (GetSystemMetrics(SM_CXSCREEN) - rcWnd.right) / 2;
	rcWnd.top  = (GetSystemMetrics(SM_CYSCREEN) - rcWnd.bottom) / 2;
	AdjustWindowRect(&rcWnd, WindowStyle, 0);
	
	WINDOWINFO wi;
	wi.cbSize = sizeof(WINDOWINFO);
	GetWindowInfo(hMainWnd, &wi);
	SetWindowPos(hMainWnd, HWND_NOTOPMOST, rcWnd.left, rcWnd.top, 
		rcWnd.right - wi.rcWindow.left + wi.rcClient.left, 
		rcWnd.bottom - wi.rcWindow.top + wi.rcClient.top, SWP_SHOWWINDOW);

	glViewport(0, 0, width, height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0, width, height, 0, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void GLWindow::Redraw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	if (RenderSceneFunc != nullptr) {
		RenderSceneFunc();
	}
	glFlush();
	SwapBuffers(hDC);
}

GLWindow::~GLWindow()
{
	wglMakeCurrent(0, 0);
	if (hRC != nullptr) {
		wglDeleteContext(hRC);
	}
	if (hDC != nullptr) {
		ReleaseDC(hMainWnd, hDC);
	}
	if (hMainWnd != nullptr) {
		DestroyWindow(hMainWnd);
	}
	if (hInstance != nullptr) {
		UnregisterClassW(WINDOW_CLASS_NAME, hInstance);
	}
}
