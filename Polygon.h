#pragma once

#include <vector>

struct Vertex {
	Vertex();
	int x, y;
	bool checked;
};

typedef std::vector<Vertex> Poly;

struct Texture;
class PolygonManager {
public:
	PolygonManager();
	~PolygonManager();

	int CalcOrientation(Poly &poly);

	void LoadPolygons(Texture *tex, float x, float y);
	void SavePolygons(Texture *tex);

	void EraseSelected(float x, float y);
	bool InverseSelected(float x, float y);

	void EraseTwoLastVertices();
	void EraseCheckedVertex();

	void UpdateLastVertex(float x, float y);
	void AddVertex(float x, float y, double rect_size);
	void RedrawPolygons(float x, float y, double rect_size, bool edit, bool button_down, long &drag_x, long &drag_y);
	void Reset(float x, float y);

private:
	bool PolySelected(float x, float y, Poly &poly);

	std::vector<Poly> Polygons;
	std::vector<int> Orientation;

};